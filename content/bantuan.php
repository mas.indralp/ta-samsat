<div class="row-fluid sortable">
<div class="box span12">
    <div class="box-header" data-original-title>
        <h2><i class="halflings-icon white user"></i><span class="break"></span>Isi Halaman Bantuan</h2>
    </div>
  
    <div class="box-content">
        <table class="table table-striped bootstrap-datatable">
            <tr>
                <th>1). Catat Pelanggaran</th>
                <th>:</th>
                <th>Digunakan untuk mencatat setiap pelanggaran yang telah dilakukan oleh siswa SMPN 1 Pilangkenceng.</th>
                
            </tr>
            <tr>
                <th>2). Identitas Sekolah</th>
                <th>:</th>
                <th>Berisi informasi Identitas SMPN 1 Pilangkenceng.</th>
                
            </tr>
            <tr>
                <th>3). List dan Isi Data Siswa</th>
                <th>:</th>
                <th>Digunakan untuk melihat daftar siswa dan menambah, merubah, menghapus data siswa SMPN 1 Pilangkenceng.</th>
                
            </tr>
            <tr>
                <th>4). List dan Isi Data Guru</th>
                <th>:</th>
                <th>Digunakan untuk melihat daftar siswa dan menambah, merubah, menghapus data Guru SMPN 1 Pilangkenceng.</th>
            </tr>
            <tr>
                <th>5). Data Pelanggaran</th>
                <th>:</th>
                <th>Digunakan untuk membuat daftar pelanggaran beserta poin pelanggaran siswa SMPN 1 Pilangkenceng.</th>
                
            </tr>
            <tr>
                <th>6). Data Hukuman</th>
                <th>:</th>
                <th>Memberikan informasi mengenai hukuman kepada siswa terhadap poin pelanggaran yang di dapat siswa.</th>
                
            </tr>
            <tr>
                <th>7). Laporan</th>
                <th>:</th>
                <th>Laporan daftar pelanggaran siswa SMPN 1 Pilangkenceng.</th>
                
            </tr>
            <tr>
                <th>8). Skor Siswa</th>
                <th>:</th>
                <th>Menampilkan jumlah keseluruhan poin pelanggaran yg didapatkan siswa SMPN 1 Pilangkenceng.</th>
                
            </tr>
            <tr>
                <th>9). Profile</th>
                <th>:</th>
                <th>Menampilkan Visi, Misi dan Profile SMPN 1 Pilangkenceng.</th>
                
            </tr>
            <tr>
                <th>10). Bantuan</th>
                <th>:</th>
                <th>Informasi bantuan aplikasi (+6281335808100).</th>
                
            </tr>
        </table>            
    </div>
 
</div><!--/span-->
</div>