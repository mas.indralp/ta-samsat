<?php 
if($_GET['id']){
    include("./koneksi.php");
    $query = $conn->prepare("Select * from t_sanksi where id=".$_GET['id']);
    $query->execute();
    $data=$query->fetch();
}

$qhk = $conn->prepare("Select * from t_sanksi");
$qhk->execute();
$dhk=$qhk->rowcount();


?>
<div class="row-fluid sortable">
    <div class="box span12">
        <div class="box-header" data-original-title>
            <h2><i class="halflings-icon white edit"></i><span class="break"></span>Form Data Sanksi / Hukuman</h2>
            
        </div>
        <div class="box-content">
            <form class="form-horizontal" method="POST" action="save/hukuman.php">
                <fieldset>

                <div class="control-group">
                    <label class="control-label">Kode Sanksi</label>
                    <div class="controls">
                       
                        <input class="input-xlarge" name="kode" type="text" placeholder="Kode Sanksi Hukuman" value=" <?php
                            if($_GET['id']){
                                echo @$data['kode']; 
                            }
                        ?>">
                        <input class="input-xlarge" name="id" type="hidden" value="<?php echo @$data['id']; ?>">
                    </div>
                </div>
            
                <div class="control-group">
                    <label class="control-label">Sanksi</label>
                    <div class="controls">
                        <input class="input-xlarge"  name="sanksi" type="text" placeholder="Sanksi Siswa" value="<?php echo @$data['sanksi']; ?>">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Poin</label>
                    <div class="controls">
                        <input class="input-small"  name="poinmin" type="text" placeholder="Poin Min" value="<?php echo @$data['poinmin']; ?>"> - 
                        <input class="input-small"  name="poinmax" type="text" placeholder="Poin Max" value="<?php echo @$data['poinmax']; ?>">
                    </div>
                </div>
               
               
                <div class="form-actions">
                    <button type="submit" class="btn btn-primary">Simpan Data</button>
                    <button type="reset" class="btn">Batal</button>
                </div>
                </fieldset>
            </form>   

        </div>
    </div><!--/span-->

</div><!--/row-->