<?php 

?>
<div class="row-fluid sortable">
    <div class="box span12">
    
        <div class="box-header" data-original-title>
            <h2><i class="halflings-icon white edit"></i><span class="break"></span>Form Laporan Data Pelanggaran Siswa Interval Tanggal</h2>
            
        </div>
        <div class="box-content">
            <form class="form-horizontal" method="POST" target="_blank" action="report/siswa.php">
                <fieldset>

                <div class="control-group">
                    <label class="control-label">Tanggal Awal</label>
                    <div class="controls">
                    <input type="text" class="input-xlarge datepicker" id="date01" name="tgla" value="">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Tanggal Akhir</label>
                    <div class="controls">
                    <input type="text" class="input-xlarge datepicker" id="date02" name="tglb" value="">
                    </div>
                </div>
               
                <div class="form-actions">
                    <button type="submit" class="btn btn-primary">Lihat Data</button>
                    
                </div>
                </fieldset>
            </form>   

        </div>
    </div><!--/span-->

</div><!--/row-->

<div class="row-fluid sortable">
    <div class="box span12">
    
        <div class="box-header" data-original-title>
            <h2><i class="halflings-icon white edit"></i><span class="break"></span>Form Laporan Data Pelanggaran Siswa Per-Siswa</h2>
            
        </div>
        <div class="box-content">
            <form class="form-horizontal" method="POST" target="_blank" action="report/siswa.php">
                <fieldset>

                <div class="control-group">
                    <label class="control-label">NISN Siswa</label>
                    <div class="controls">
                        <input class="input-xlarge" name="nisn" type="text" placeholder="Nisn Siswa" value="<?php  ?>">
                    </div>
                </div>
               
                <div class="form-actions">
                    <button type="submit" class="btn btn-primary">Lihat Data</button>
                    
                </div>
                </fieldset>
            </form>   

        </div>
    </div><!--/span-->

</div><!--/row-->