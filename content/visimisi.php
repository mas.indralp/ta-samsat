<?php 
if($_GET['id']){
    $query = $conn->prepare("Select * from t_sekolah where id=".$_GET['id']);
    $query->execute();
    $data=$query->fetch();
}
?>
<div class="row-fluid sortable">
    <div class="box span12">
        <div class="box-header" data-original-title>
            <h2><i class="halflings-icon white edit"></i><span class="break"></span>Form Visi Misi Sekolah</h2>
        </div>
        <div class="box-content">
            <form class="form-horizontal" method="POST" action="save/visimisi.php">
                <fieldset>

                <div class="control-group">
                    <label class="control-label">VISI</label>
                    <div class="controls">
                    <textarea class="cleditor" id="textarea2" rows="3" name="visi"><?php echo $data['visi']; ?></textarea>
                    <input class="input-xlarge" name="id" type="hidden" value="<?php echo $data['id']; ?>">
                  
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">MISI</label>
                    <div class="controls">
                    <textarea class="cleditor" id="textarea2" rows="3" name="misi"><?php echo $data['misi']; ?></textarea>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Profile</label>
                    <div class="controls">
                    <textarea class="cleditor" id="textarea2" rows="3" name="profile"><?php echo $data['profile']; ?></textarea>
                    </div>
                </div>
               
                <div class="form-actions">
                    <button type="submit" class="btn btn-primary">Simpan Data</button>
                    <button type="reset" class="btn">Batal</button>
                </div>
                </fieldset>
            </form>   

        </div>
    </div><!--/span-->

</div><!--/row-->