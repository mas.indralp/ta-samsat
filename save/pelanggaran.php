<!-- Simpan, Edit, Hapus -->
<?php
include("../koneksi.php");
if($_POST['id']){
    //Update data 
     
     $sql=$conn->prepare("Update t_pelanggaran set kode=:kd,nama=:nm,poin=:poin where id=:id");
     $data=array(
        ':id'=>$_POST['id'],
        ':kd'=>$_POST['kode'],
        ':nm'=>$_POST['nama'],
        ':poin'=>$_POST['poin'],
     );
     $sql->execute($data);
}else{
    //Simpan data baru
    $sql=$conn->prepare("Insert into t_pelanggaran (kode,nama,poin) values(:kd,:nm,:poin)");
    $data=array(
        ':kd'=>$_POST['kode'],
        ':nm'=>$_POST['nama'],
        ':poin'=>$_POST['poin'],
    );

    $sql->execute($data);
   
}
header("Location: http://localhost/ta/index.php?page=ListPelanggaran");
exit;

?>