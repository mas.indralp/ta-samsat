<!-- Simpan, Edit, Hapus -->
<?php
include("../koneksi.php");
if($_POST['id']){
    //Update data 
     
     $sql=$conn->prepare("Update t_catatan set kd_pelanggaran=:kd,poin=:poin,nipd=:nipd,tanggal=:tgl where id=:id");
     $data=array(
        ':id'=>$_POST['id'],
        ':poin'=>$_POST['poin'],
        ':kd'=>substr($_POST['kdpel'],0,strpos($_POST['kdpel'],'-')),
        ':nipd'=>$_POST['nipd'],
        ':tgl'=>date('Y-m-d',strtotime($_POST['tglpel'])),
     );
     $sql->execute($data);
}else{
    //Simpan data baru
    $sql=$conn->prepare("Insert into t_catatan (kd_pelanggaran,poin,nipd,tanggal) values(:kd,:poin,:nipd,:tgl)");
    $data=array(
        ':kd'=>substr($_POST['kdpel'],0,strpos($_POST['kdpel'],'-')),
        ':poin'=>$_POST['poin'],
        ':nipd'=>$_POST['nipd'],
        ':tgl'=>date('Y-m-d',strtotime($_POST['tglpel'])),
       
    );

    $sql->execute($data);
   
}
header("Location: http://localhost/ta/index.php?page=CatatPelanggaran");
exit;

?>
