<!-- Simpan, Edit, Hapus -->
<?php
include("../koneksi.php");
if($_POST['id']){
    //Update data 
     
     $sql=$conn->prepare("Update t_sanksi set nama=:nama,npsn=:npsn,nss=:nss,alamat=:alamat,telp=:telp,kelurahan=:kelu,kecamatan=:keca,kota=:kota,provinsi=:prov,web:web,email=:email where id=:id");
     $data=array(
        ':id'=>$_POST['id'],
        ':nama'=>$_POST['nama'],
        ':npsn'=>$_POST['npsn'],
        ':nss'=>$_POST['nss'],
        ':alamat'=>$_POST['alamat'],
        ':telp'=>$_POST['telp'],
        ':kelu'=>$_POST['kelurahan'],
        ':keca'=>$_POST['kecamatan'],
        ':kota'=>$_POST['kota'],
        ':prov'=>$_POST['provinsi'],
        ':web'=>$_POST['web'],
        ':email'=>$_POST['email'],

     );
     $sql->execute($data);
}else{
    //Simpan data baru
    $sql=$conn->prepare("Insert into t_identitas (nama,npsn,nss,alamat,telp,kelurahan,kecamatan,kota,provinsi,web,email) values(:nama,:npsn,:nss,:alamat,:telp,:kelu,:keca,:kota,:prov,:web,:email)");
    $data=array(
        ':nama'=>$_POST['nama'],
        ':npsn'=>$_POST['npsn'],
        ':nss'=>$_POST['nss'],
        ':alamat'=>$_POST['alamat'],
        ':telp'=>$_POST['telp'],
        ':kelu'=>$_POST['kelurahan'],
        ':keca'=>$_POST['kecamatan'],
        ':kota'=>$_POST['kota'],
        ':prov'=>$_POST['provinsi'],
        ':web'=>$_POST['web'],
        ':email'=>$_POST['email'],

    );

    $sql->execute($data);
   
}
header("Location: http://localhost/ta/index.php");
exit;

?>