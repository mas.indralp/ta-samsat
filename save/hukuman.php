<!-- Simpan, Edit, Hapus -->
<?php
include("../koneksi.php");
if($_POST['id']){
    //Update data 
     
     $sql=$conn->prepare("Update t_sanksi set kode=:kd,sanksi=:sanksi,poinmin=:poinmin,poinmax=:poinmax where id=:id");
     $data=array(
        ':id'=>$_POST['id'],
        ':kd'=>$_POST['kode'],
        ':sanksi'=>$_POST['sanksi'],
        ':poinmin'=>$_POST['poinmin'],
        ':poinmax'=>$_POST['poinmax'],
     );
     $sql->execute($data);
}else{
    //Simpan data baru
    $sql=$conn->prepare("Insert into t_sanksi (kode,sanksi,poinmin,poinmax) values(:kd,:sanksi,:poinmin,:poinmax)");
    $data=array(
        ':kd'=>$_POST['kode'],
        ':sanksi'=>$_POST['sanksi'],
        ':poinmin'=>$_POST['poinmin'],
        ':poinmax'=>$_POST['poinmax'],
    );

    $sql->execute($data);
   
}
header("Location: http://localhost/ta/index.php?page=ListHukuman");
exit;

?>
