<!DOCTYPE html>
<html lang="en">
<head>
	<?php session_start(); 
	
	if(@$_GET['page']=='logout'){
		session_destroy();
		header("Location: http://localhost/ta/login.php");
		exit();
	}
	if(!$_SESSION['nama']){header("Location: http://localhost/ta/login.php");  }
	?> 
	<!-- start: Meta -->
	<meta charset="utf-8">
	<title>Dashboard Admin</title>
	<meta name="description" content="Bootstrap Dashboard">
	<meta name="author" content="Indra Laksana Putra">
	<meta name="keyword" content="Metro, Metro UI, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
	<!-- end: Meta -->
	
	<!-- start: Mobile Specific -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- end: Mobile Specific -->
	
	<?php include("css.php");   
	include("koneksi.php");
	$actual_link = "http://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";
	?>
	

	<!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
	  	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<link id="ie-style" href="css/ie.css" rel="stylesheet">
	<![endif]-->
	
	<!--[if IE 9]>
		<link id="ie9style" href="css/ie9.css" rel="stylesheet">
	<![endif]-->
		
	<!-- start: Favicon -->
	<link rel="shortcut icon" href="img/favicon.ico">
	<!-- end: Favicon -->
		
		
</head>

<body>
	<!-- start: Header -->
	<div class="navbar">
		<?php include("header/header.php"); ?>
	</div>
	<!-- start: Header -->
	
		<div class="container-fluid-full">
		<div class="row-fluid">
				
			<!-- start: Main Menu -->
			<?php include("menu/menu.php"); ?>
			<!-- end: Main Menu -->
			
			<noscript>
				<div class="alert alert-block span10">
					<h4 class="alert-heading">Warning!</h4>
					<p>You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a> enabled to use this site.</p>
				</div>
			</noscript>
			
			<!-- start: Content -->
			<div id="content" class="span10">
			
			
			<ul class="breadcrumb">
				<li>
					<i class="icon-home"></i>
					<a href="index.html">Home</a> 
					<i class="icon-angle-right"></i>
				</li>
				
				<li><a href="#">
				<?php
				error_reporting(0);
				echo "Data ". $_GET['page'];
				?>
				</a></li>
			</ul>

			<div class="row-fluid">
				<?php
				error_reporting(0);
					switch ($_GET['page']) {
						case "CatatPelanggaran":
							include("content/catatpel.php");
							break;
						case "Identitas":
							include("content/identitas.php");
							break;
						case "Kelas":
							include("content/kelas.php");
							break;
						case "ListKelas":
							include("view/kelas.php");
							break;
						case "EditKelas":
							include("content/kelas.php");
							break;
						case "VisiMisi":
							include("content/visimisi.php");
							break;
						case "Siswa":
							include("content/siswa.php");
							break;
						case "ListSiswa":
							include("view/siswa.php");
							break;
						case "EditSiswa":
							include("content/siswa.php");
							break;
						case "Guru":
							include("content/guru.php");
							break;
						case "ListGuru":
							include("view/guru.php");
							break;
						case "EditGuru":
							include("content/guru.php");
							break;
						case "Pelanggaran":
							include("content/pelanggaran.php");
							break;
						case "EditPelanggaran":
							include("content/pelanggaran.php");
							break;
						case "ListPelanggaran":
							include("view/pelanggaran.php");
							break;
						case "ListHukuman":
							include("view/hukuman.php");
							break;
						case "Hukuman":
							include("content/hukuman.php");
							break;
						case "EditHukuman":
							include("content/hukuman.php");
							break;
						case "Laporan":
							include("content/laporan.php");
							break;
						case "SkorSiswa":
							include("view/skorsiswa.php");
							break;
						case "Profile":
							include("content/visimisi.php");
							break;
						case "Bantuan":
							include("content/bantuan.php");
							break;
						default:
							include("content/home.php");
							break;
					}

				
				?>

				
				<div class="clearfix"></div>
								
			</div><!--/row-->
			
       

	</div><!--/.fluid-container-->
	
			<!-- end: Content -->
		</div><!--/#content.span10-->
		</div><!--/fluid-row-->
		
	
	
	<div class="clearfix"></div>
	
	<footer>

		<p>
			<span style="text-align:left;float:left">&copy; 2019 <a href="http://www.idwebbuilder.com" alt="">Sistem Informasi Pelanggaran Siswa SMPN 1 Pilangkenceng. All right reserved.</a></span>
			
		</p>

	</footer>
	
	<!-- start: JavaScript-->

		<script src="js/jquery-1.9.1.min.js"></script>
	<script src="js/jquery-migrate-1.0.0.min.js"></script>
	
		<script src="js/jquery-ui-1.10.0.custom.min.js"></script>
	
		<script src="js/jquery.ui.touch-punch.js"></script>
	
		<script src="js/modernizr.js"></script>
	
		<script src="js/bootstrap.min.js"></script>
	
		<script src="js/jquery.cookie.js"></script>
	
		<script src='js/fullcalendar.min.js'></script>
	
		<script src='js/jquery.dataTables.min.js'></script>

		<script src="js/excanvas.js"></script>
	<script src="js/jquery.flot.js"></script>
	<script src="js/jquery.flot.pie.js"></script>
	<script src="js/jquery.flot.stack.js"></script>
	<script src="js/jquery.flot.resize.min.js"></script>
	
		<script src="js/jquery.chosen.min.js"></script>
	
		<script src="js/jquery.uniform.min.js"></script>
		
		<script src="js/jquery.cleditor.min.js"></script>
	
		<script src="js/jquery.noty.js"></script>
	
		<script src="js/jquery.elfinder.min.js"></script>
	
		<script src="js/jquery.raty.min.js"></script>
	
		<script src="js/jquery.iphone.toggle.js"></script>
	
		<script src="js/jquery.uploadify-3.1.min.js"></script>
	
		<script src="js/jquery.gritter.min.js"></script>
	
		<script src="js/jquery.imagesloaded.js"></script>
	
		<script src="js/jquery.masonry.min.js"></script>
	
		<script src="js/jquery.knob.modified.js"></script>
	
		<script src="js/jquery.sparkline.min.js"></script>
	
		<script src="js/counter.js"></script>
	
		<script src="js/retina.js"></script>

		<script src="js/custom.js"></script>
	<!-- end: JavaScript-->
	
	<script>
		$('#pelanggaran').change(function(){
			var poin = $('#pelanggaran').val();
			
			var awal = poin.search("-");
			var akhir =poin.length;

			var hsl = poin.substring(awal + 1, akhir);
			$('#nilaipoin').val(hsl);
		});
	</script>
	<script>
	function gantisiswa(){
			$.get("http://localhost/ta/view/cekkelas.php?kelas="+$("#kelas").val(),function(data){
$("#siswapel").empty();				
$("#siswapel").html(data);
$("#siswapel").trigger("liszt:updated");
			});

	}
	</script>


</body>
</html>
