-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.33-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for bk
DROP DATABASE IF EXISTS `bk`;
CREATE DATABASE IF NOT EXISTS `bk` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `bk`;

-- Dumping structure for table bk.t_catatan
DROP TABLE IF EXISTS `t_catatan`;
CREATE TABLE IF NOT EXISTS `t_catatan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kd_pelanggaran` varchar(50) DEFAULT NULL,
  `poin` varchar(50) DEFAULT NULL,
  `nipd` varchar(50) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- Dumping data for table bk.t_catatan: ~3 rows (approximately)
/*!40000 ALTER TABLE `t_catatan` DISABLE KEYS */;
INSERT INTO `t_catatan` (`id`, `kd_pelanggaran`, `poin`, `nipd`, `tanggal`) VALUES
	(6, 'C1', '10', '12', '2019-08-01'),
	(7, 'C2', '50', '12', '2019-08-04'),
	(8, 'C1', '10', '14', '2019-08-02');
/*!40000 ALTER TABLE `t_catatan` ENABLE KEYS */;

-- Dumping structure for table bk.t_guru
DROP TABLE IF EXISTS `t_guru`;
CREATE TABLE IF NOT EXISTS `t_guru` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nip` varchar(50) DEFAULT NULL,
  `nama` varchar(50) DEFAULT NULL,
  `jk` varchar(50) DEFAULT NULL,
  `telp` varchar(50) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `pass` longtext,
  PRIMARY KEY (`id`),
  KEY `nip` (`nip`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Dumping data for table bk.t_guru: ~0 rows (approximately)
/*!40000 ALTER TABLE `t_guru` DISABLE KEYS */;
INSERT INTO `t_guru` (`id`, `nip`, `nama`, `jk`, `telp`, `status`, `pass`) VALUES
	(5, '1', '2', 'P', '3', 'pns', '202cb962ac59075b964b07152d234b70');
/*!40000 ALTER TABLE `t_guru` ENABLE KEYS */;

-- Dumping structure for table bk.t_identitas
DROP TABLE IF EXISTS `t_identitas`;
CREATE TABLE IF NOT EXISTS `t_identitas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) DEFAULT NULL,
  `npsn` varchar(50) DEFAULT NULL,
  `nss` varchar(50) DEFAULT NULL,
  `alamat` varchar(50) DEFAULT NULL,
  `telp` varchar(50) DEFAULT NULL,
  `kelurahan` varchar(50) DEFAULT NULL,
  `kecamatan` varchar(50) DEFAULT NULL,
  `kota` varchar(50) DEFAULT NULL,
  `provinsi` varchar(50) DEFAULT NULL,
  `web` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table bk.t_identitas: ~0 rows (approximately)
/*!40000 ALTER TABLE `t_identitas` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_identitas` ENABLE KEYS */;

-- Dumping structure for table bk.t_kelas
DROP TABLE IF EXISTS `t_kelas`;
CREATE TABLE IF NOT EXISTS `t_kelas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kelas` varchar(50) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;

-- Dumping data for table bk.t_kelas: ~27 rows (approximately)
/*!40000 ALTER TABLE `t_kelas` DISABLE KEYS */;
INSERT INTO `t_kelas` (`id`, `kelas`) VALUES
	(1, 'VII-A'),
	(2, 'VII-B'),
	(3, 'VII-C'),
	(4, 'VII-D'),
	(5, 'VII-E'),
	(6, 'VII-F'),
	(7, 'VII-G'),
	(8, 'VII-H'),
	(9, 'VII-I'),
	(10, 'VIII-A'),
	(11, 'VIII-B'),
	(12, 'VIII-C'),
	(13, 'VIII-D'),
	(14, 'VIII-E'),
	(15, 'VIII-F'),
	(16, 'VIII-G'),
	(17, 'VIII-H'),
	(18, 'VIII-I'),
	(19, 'IX-A'),
	(20, 'IX-B'),
	(21, 'IX-C'),
	(22, 'IX-D'),
	(23, 'IX-E'),
	(24, 'IX-F'),
	(25, 'IX-G'),
	(26, 'IX-H'),
	(27, 'IX-I');
/*!40000 ALTER TABLE `t_kelas` ENABLE KEYS */;

-- Dumping structure for table bk.t_pelanggaran
DROP TABLE IF EXISTS `t_pelanggaran`;
CREATE TABLE IF NOT EXISTS `t_pelanggaran` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kode` varchar(50) DEFAULT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `poin` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `kode` (`kode`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;

-- Dumping data for table bk.t_pelanggaran: ~23 rows (approximately)
/*!40000 ALTER TABLE `t_pelanggaran` DISABLE KEYS */;
INSERT INTO `t_pelanggaran` (`id`, `kode`, `nama`, `poin`) VALUES
	(3, 'R1', 'Tidak masuk kerja tanpa alasan yang sah selama 5 h', 1),
	(4, 'R2', 'Tidak masuk kerja tanpa alasan yang sah selama 6 s/d 10 hari kerja', 1),
	(5, 'R3', 'Tidak masuk kerja tanpa alasan yang sah  selama 11 s/d 15 hari kerja', 1),
	(6, 'R4', 'tidak memakai atribut seragam sesuai  ketentuan', 1),
	(7, 'R5', 'Tidak mengikuti apel pagi', 1),
	(8, 'R6', 'Tidak mengikuti apel sore', 1),
	(9, 'R7', 'Tidak memakai seragam sesuai jadwal', 1),
	(10, 'S1', 'Tidak masuk kerja tanpa alasan yang sah selama 16 s/d 20 hari kerja', 2),
	(11, 'S2', 'Tidak masuk kerja tanpa alasan yang sah selama 21 s/d 25 hari kerja', 2),
	(12, 'S3', 'Tidak masuk kerja tanpa alasan yang sah selama 26 s/d 30 hari kerja', 2),
	(13, 'S4', 'Tidak hadir dalam pengarahan', 2),
	(14, 'S5', 'Tidak mengikuti senam pagi', 2),
	(15, ' S6', 'Tanpa ijin memakai fasilitas kantor untuk kepentingan pribadi', 2),
	(16, ' S7', 'Tidak melaksanakan perintah atasan langsung', 2),
	(17, ' S8', 'Tidak mengumpulkan hasil dinas luar sesuai ketentuan', 2),
	(18, ' B1', 'Tidak masuk kerja tanpa alasan yang sah selama 31 s/d 35 hari kerja', 3),
	(19, ' B2', 'Tidak masuk kerja tanpa alasan yang sah selama 36 s/d 40 hari kerja', 3),
	(20, ' B3', 'Tidak masuk kerja tanpa alasan yang sah selama 41 s/d 45 hari kerja', 3),
	(21, ' B5', 'Tidak masuk kerja tanpa alasan yang sah selama 46 hari kerja atau lebih', 3),
	(22, ' B4', 'Melakukan pencurian/penggelapan barang milik kantor', 3),
	(23, ' B6', 'Terbukti melakukan tindak pidana ', 3),
	(24, ' B7', 'Memakai narkoba', 3),
	(25, ' B8', 'Melakukan penggelapan uang wajib pajak', 3);
/*!40000 ALTER TABLE `t_pelanggaran` ENABLE KEYS */;

-- Dumping structure for table bk.t_sanksi
DROP TABLE IF EXISTS `t_sanksi`;
CREATE TABLE IF NOT EXISTS `t_sanksi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kode` varchar(50) DEFAULT NULL,
  `sanksi` varchar(50) DEFAULT NULL,
  `poinmin` int(11) DEFAULT NULL,
  `poinmax` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `kode` (`kode`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

-- Dumping data for table bk.t_sanksi: ~2 rows (approximately)
/*!40000 ALTER TABLE `t_sanksi` DISABLE KEYS */;
INSERT INTO `t_sanksi` (`id`, `kode`, `sanksi`, `poinmin`, `poinmax`) VALUES
	(1, 'S1', 'TEGURAN LISAN', 1, 3),
	(2, 'S2', 'TEGURAN TERTULIS', 4, 5),
	(3, 'S3', 'PERNYATAAN TIDAK PUAS SECARA TERTULIS', 6, 7),
	(4, 'S4', 'Penundaan kenaikan gaji berkala selama 1 tahun', 8, 12),
	(5, 'S5', 'Penundaan Kenaikan pangkat selama 1 tahun', 13, 18),
	(6, 'S6', 'Penurunan pangkat setingkat lebih rendah selama 1 ', 19, 23),
	(7, 'S7', 'Penurunan pangkat setingkat lebih rendah selama 3 ', 24, 30),
	(8, 'S8', 'Pemindahan dalam rangka penurunan jabatan setingka', 31, 36),
	(9, 'S9', 'Pembebasan dari jabatan bagi PNS ', 37, 42),
	(10, 'S10', 'Pemberhentian dengan hormat tidak atas permintaan ', 43, 47);
/*!40000 ALTER TABLE `t_sanksi` ENABLE KEYS */;

-- Dumping structure for table bk.t_sekolah
DROP TABLE IF EXISTS `t_sekolah`;
CREATE TABLE IF NOT EXISTS `t_sekolah` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `visi` longtext NOT NULL,
  `misi` longtext NOT NULL,
  `profile` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table bk.t_sekolah: ~0 rows (approximately)
/*!40000 ALTER TABLE `t_sekolah` DISABLE KEYS */;
INSERT INTO `t_sekolah` (`id`, `visi`, `misi`, `profile`) VALUES
	(1, '<div align="justify">Unggul dalam mutu Berdasarkan iman dan Taqwa, Berpijak Pada Budaya Bangsa Serta Berwawasan Kelestarian Lingkungan.</div>', '<ol><li>Melaksanakan peningkatan keimanan dan ketaqwaaan  kepada Tuhan Yang Maha Esa. <br></li><li>Melaksanakan Pembelajaran yang efektif dan efisien.</li><li>Melaksanakan Pembelajaran Berbasis IT.</li><li>Melaksanakan Pembinaan dalam bidang Olimpiade. <br></li><li>Melaksanakan Pengembangan Media Pembelajaran.</li><li>Melaksanakan Pembiasaan Gemar Membaca. <br></li><li>Melaksanakan Pembinaan dalam ekstrakulikuler. <br></li><li>Melaksanakan pola pengelolaan sekolah sesuai dengan MBS dan standar Menejemen Mutu <br></li><li>Melaksanakan Peningkatan kompetensi SDM <br></li><li>Meningkatkan Kesadaran dan Budaya peduli lingkungan menuju sekolah clean, green, and healthy <br></li><li>Menjalin kerjasama dengan seluruh stake holder <br></li><li>Melaksanakan kerja sama dengan Sekolah lain dan Instansi lain.\r\n</li></ol>', '<div align="justify">SMPN 01 Pilangkenceng adalah sekolah tingkat dasar yang berstandart Nasional yang berada di Desa Luworo, Kecamatan Pilangkenceng, Kabupaten Madiun yang dipimpin oleh Bapak Sumardjono, S.Pd dan beranggotakan 44 Tenaga Pendidik dan 13 Tenaga Kependidikan</div>');
/*!40000 ALTER TABLE `t_sekolah` ENABLE KEYS */;

-- Dumping structure for table bk.t_siswa
DROP TABLE IF EXISTS `t_siswa`;
CREATE TABLE IF NOT EXISTS `t_siswa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nipd` varchar(50) DEFAULT NULL,
  `nisn` varchar(50) DEFAULT NULL,
  `nama` varchar(50) DEFAULT NULL,
  `kelas` varchar(50) DEFAULT NULL,
  `angkatan` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `nipd` (`nipd`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- Dumping data for table bk.t_siswa: ~2 rows (approximately)
/*!40000 ALTER TABLE `t_siswa` DISABLE KEYS */;
INSERT INTO `t_siswa` (`id`, `nipd`, `nisn`, `nama`, `kelas`, `angkatan`) VALUES
	(6, '12', '2', 'Deni', 'VII-A', '2019'),
	(7, '14', '4', 'Dasa', 'VII-A', '2018');
/*!40000 ALTER TABLE `t_siswa` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
