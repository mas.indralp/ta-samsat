<?php 
    if(@$_GET['del']){
        $query = $conn->prepare("Delete from t_kurikulum where id='".$_GET['del']."'");
        $query->execute();
    }
    $query = $conn->prepare("Select * from t_kurikulum");
    $query->execute();
?>

<div class="row-fluid sortable">
<div class="box span12">
    <button type="submit" class="btn btn-primary" onclick="window.location.href='http://localhost/sekolah/index.php?page=Kurikulum'">Tambah Data</button><p>
    <div class="box-header" data-original-title>
        <h2><i class="halflings-icon white user"></i><span class="break"></span>Daftar Kurikulum</h2>
       
    </div>
  
    <div class="box-content">
        <table class="table table-striped table-bordered bootstrap-datatable datatable">
            <thead>
                <tr>
                    <th>Kurikulum</th>
                    <th>Status</th>
                    <th>Actions</th>
                </tr>
            </thead>   
            <tbody>
                <?php while($data = $query->fetch()){ ?>
                <tr>	
                   
                    <td><?php echo $data['kurikulum']; ?></td>
                    <td><?php echo $data['status']; ?></td>
                   
                    <td class="center"> 
                        <a class="btn btn-info" href="index.php?page=EditKurikulum&id=<?php echo $data['id']; ?>">
                            <i class="halflings-icon white edit"></i>  
                        </a>
                        <a class="btn btn-danger" href="<?php echo $actual_link; ?>&del=<?php echo $data['id']; ?>">
                            <i class="halflings-icon white trash"></i> 
                        </a>
                    </td>
                </tr>
                <?php } ?>
            </tbody>
        </table>            
    </div>
 
</div><!--/span-->
</div>