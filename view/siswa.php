<?php 
    
    if(@$_GET['del']){
        $query = $conn->prepare("Delete from t_siswa where nipd='".$_GET['del']."'");
        $query->execute();
    }

    $query = $conn->prepare("Select * from t_siswa");
    $query->execute();


?>


<div class="row-fluid sortable">
<div class="box span12">
    <div class="box-header" data-original-title>
        <h2><i class="halflings-icon white user"></i><span class="break"></span>Daftar Siswa</h2>
    </div>
  
    <div class="box-content">
        <table class="table table-striped table-bordered bootstrap-datatable datatable">
            <thead>
                <tr>
                    <th>NIPD</th>
                    <th>NISN</th>
                    <th>Nama Siswa</th>
                    <th>Kelas</th>
                    <th>Angkatan</th>
                    <th>Actions</th>
                </tr>
            </thead>   
            <tbody>
                <?php while($data = $query->fetch()){ ?>
                <tr>	
                   
                    <td><?php echo $data['nipd']; ?></td>
                    <td><?php echo $data['nisn']; ?></td>
                    <td><?php echo $data['nama']; ?></td>
                    <td><?php echo $data['kelas']; ?></td>
                    <td><?php echo $data['angkatan']; ?></td>
                
                    <td class="center"> 
                        <a class="btn btn-info" href="index.php?page=EditSiswa&nipd=<?php echo $data['nipd']; ?>">
                            <i class="halflings-icon white edit"></i>  
                        </a>
                        <a class="btn btn-danger" href="<?php echo $actual_link; ?>&del=<?php echo $data['nipd']; ?>">
                            <i class="halflings-icon white trash"></i> 
                        </a>
                    </td>
                </tr>
                <?php } ?>
            </tbody>
        </table>            
    </div>
 
</div><!--/span-->
</div>

