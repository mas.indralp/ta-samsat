<?php 
    if(@$_GET['del']){
        $query = $conn->prepare("Delete from t_pelanggaran where id='".$_GET['del']."'");
        $query->execute();
    }
    $query = $conn->prepare("Select * from t_pelanggaran");
    $query->execute();
?>

<div class="row-fluid sortable">
<div class="box span12">
<button type="submit" class="btn btn-primary" onclick="window.location.href='http://localhost/ta/index.php?page=Pelanggaran'">Tambah Data</button><p>
    
    <div class="box-header" data-original-title>
        <h2><i class="halflings-icon white user"></i><span class="break"></span>Daftar Pelanggaran</h2>
       
    </div>
  
    <div class="box-content">
        <table class="table table-striped table-bordered bootstrap-datatable datatable">
            <thead>
                <tr>
                    <th>Kode</th>
                    <th>Nama Pelanggaran</th>
                    <th>Poin Pelanggaran</th>
                    <th>Actions</th>
                </tr>
            </thead>   
            <tbody>
                <?php while($data = $query->fetch()){ ?>
                <tr>	
                   
                    <td><?php echo $data['kode']; ?></td>
                    <td><?php echo $data['nama']; ?></td>
                    <td><?php echo $data['poin']; ?></td>
                 
                    <td class="center"> 
                        <a class="btn btn-info" href="index.php?page=EditPelanggaran&id=<?php echo $data['id']; ?>">
                            <i class="halflings-icon white edit"></i>  
                        </a>
                        <a class="btn btn-danger" href="<?php echo $actual_link; ?>&del=<?php echo $data['id']; ?>">
                            <i class="halflings-icon white trash"></i> 
                        </a>
                    </td>
                </tr>
                <?php } ?>
            </tbody>
        </table>            
    </div>
 
</div><!--/span-->
</div>